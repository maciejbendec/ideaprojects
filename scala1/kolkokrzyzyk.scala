/**
  * Created by maciej on 21.03.17.
  */

class Plansza{
  var Pola = Array(0,0,0,0,0,0,0,0,0)
  var turn=1 //1-kolko, 2 - krzyzyk
  var koniec=0
  def rysujPlansze(){
    var x=0
    while(x<9){
        if(Pola(x)==0) print(" ")
        else if(Pola(x)==1) print("O")
        else print("X")
        if(x%3==0 || (x+2)%3==0) print(" | ")
        else println("")
        if((x+1)%3==0 && x!=8) println("----------")
        x=x+1
    }
  }
  def sprawdzWygrana(){
    var x=0
    var wygrana=0
    while(x<3){//poziom
      if((Pola(x)==turn)&&(Pola(x+1)==turn)&&(Pola(x+2)==turn)) {
        wygrana = 1
      }
      if((Pola(x)==turn)&&(Pola(x+3)==turn)&&(Pola(x+6)==turn)) {
        wygrana = 1
      }

      x=x+1
    }
    if((Pola(0)==turn)&&(Pola(4)==turn)&&(Pola(8)==turn)) {
      wygrana = 1
    }
    if((Pola(2)==turn)&&(Pola(4)==turn)&&(Pola(6)==turn)) {
      wygrana = 1
    }
    if(wygrana==1){
      if(turn==1) println("Wygrywa kolko!")
      else println("Wygrywa krzyzyk!")
      koniec=1
    }
  }
  def zaznaczPole(pole: Int){
    Pola(pole)=turn
    sprawdzWygrana()
    if(turn==1) turn =2
    else turn=1
  }
}
var mojaPlansza= new Plansza
mojaPlansza.rysujPlansze()
var licznik=0
var pole=0
while(licznik<9){
  println("Podaj pole")
  try {
    pole = Console.readInt() - 1
    if(pole<0 || pole>8){
      println("Liczba poza zakresem 1-9!")
    }
    else if(mojaPlansza.Pola(pole)!=0){
      println("Zajęte!")
    }
    else{
      mojaPlansza.zaznaczPole(pole)
      licznik=licznik+1
      mojaPlansza.rysujPlansze()
      if (mojaPlansza.koniec==1) licznik=10
    }
  }
  catch{
    case _: Throwable => println("To nie jest liczba!")
  }

}
if(mojaPlansza.koniec==0)
  println("Remis!")