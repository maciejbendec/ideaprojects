/**
  * Created by maciej on 28.03.17.
  */

val movies = <movies><movie genre="action">Pirates of the Caribbean</movie>
  <movie genre="fairytale">Edward Scissorhands</movie></movies>
println(movies.text)
val movieNodes = movies \ "movie"
println(movieNodes(0))
println(movieNodes(0) \ "@genre")