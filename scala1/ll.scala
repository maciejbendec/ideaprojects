/**
  * Created by maciej on 28.03.17.
  */

val animals = Set("lions", "tigers", "bears")

println(animals)

println(animals+"armadillos")


var animals2 = animals

animals2=animals2-"tigers"

println(animals2)

var animals3 = animals

//animals + Set("armadillos", "raccoons")​

animals3=animals3 ++ Set("armadillos", "raccoons")

println(animals3)


var a = Set(1, 2, 3)
var b = Set(3, 2, 1)

if(a==b){
  println("True")
} else {
  println("False")
}

val hobbits = Set("frodo", "samwise", "pippin")
hobbits.foreach(hobbit => println(hobbit))