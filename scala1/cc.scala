/**
  * Created by maciej on 21.03.17.
  */

object Test {
  def main(args: Array[String]){
    var a=1;
    var i=0;
    println("while loop java style")
    while(a < 10){
      println("Value of a: "+ a);
      a=a+1
    }
    println("for loop java style")
    for(i <- 0 until args.length){
      println(args(i))
    }
    println("for loop ruby style")
    args.foreach {arg=>
        println(arg)
    }
  }
}

Test.main(Array("a","b","c"))