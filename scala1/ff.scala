/**
  * Created by maciej on 21.03.17.
  */

class Person(firstName: String){
  println("Outer constructior")
  def this(firstName: String,lastName: String){
    this(firstName)
    println("Inner constructor")
  }

}
val bob = new Person("Bob")
val bobTate = new Person("Bob", "Tate")
